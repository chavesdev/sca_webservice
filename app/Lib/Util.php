<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * Description of Util
 *
 * @author rodrigo_chaves
 */
class Util {
    /**
     * Alternative function to encode array to json
     * @param $val
     * @return string
     */
    static function my_json_encode($val){
         if (is_string($val)) return '"'.addslashes($val).'"';
         if (is_numeric($val)) return $val;
         if ($val === null) return 'null';
         if ($val === true) return 'true';
         if ($val === false) return 'false';

         $assoc = false;
         $i = 0;
         foreach ($val as $k=>$v){
             if ($k !== $i++){
                 $assoc = true;
                 break;
             }
         }
         $res = array();
         foreach ($val as $k=>$v){
             $v = self::my_json_encode($v);
             if ($assoc){
                 $k = '"'.addslashes($k).'"';
                 $v = $k.':'.$v;
             }
             $res[] = $v;
         }
         $res = implode(',', $res);

        $my_return = ($assoc)? '{'.$res.'}' : '['.$res.']';

         return $my_return;

         }

    /**
     * rename the file by sha1 and removing acents
     * @param null $filename
     * @return string
     */
    static function clean_filename($filename = null){
        require_once 'URLify.php';
        $newname = '';
        $parts_name = explode('.',$filename);
        $extension = $parts_name[(count($parts_name)-1)];
        $filename = $parts_name[0];
        $newname = sha1(URLify::downcode($filename));
        $newname .='.'.$extension;
        return $newname;
    }


    /**
     * format br money from string to decimal(10,2)
     * @param null $money
     * @return mixed|string
     */
    static function dec2_string($money = null){
        $money_decimal = "";
        if(strpos($money,'.')){//if bigger than 999,00
            $p_real = explode('.',$money);
            $p_cents = explode(',',$p_real[1]);

            $money_decimal = $p_real[0] . $p_cents[0] . '.' . $p_cents[1];
        }
        else{
            $money_decimal = str_replace(',','.',$money);
        }

        return $money_decimal;
    }
}

?>
