<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 22/11/15
 * Time: 22:49
 */

class Aluno extends AppModel{
    public $actsAs = array('Containable');

    public $belongsTo = array(
      'Curso' => array(
          'className' => 'Curso',
          'foreignKey' => 'curso_id'
      )
    );

    public $hasAndBelongsToMany = array(
        'Disciplina' => array(
            'className' => 'Disciplina',
            'joinTable' => 'alunos_tem_disciplinas',
            'foreignKey' => 'aluno_id',
            'associationForeignKey' => 'disciplina_id',
        )
    );
} 