<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 23/11/15
 * Time: 03:04
 */

class Disciplina extends AppModel{

    public $hasAndBelongsToMany = array(
        'Aluno' => array(
            'className' => 'Aluno',
            'joinTable' => 'alunos_tem_disciplinas',
            'foreignKey' => 'disciplina_id',
            'associationForeignKey' => 'aluno_id',
        )
    );
} 