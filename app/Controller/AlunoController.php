<?php

App::uses('Util','Lib');
App::uses('URLify','Lib');

class AlunoController extends AppController{

    /**
     * Verifica login com cpf e matricula
     */
    public function login(){
        $this->layout = 'ajax';
        $val_return = array('status'=>'false','msg'=>'Você precisa estar logado');

        if($this->request->is('post')){
            //pr($this->request->data);
            if(!empty($this->request->data['Aluno']['cpf']) && !empty($this->request->data['Aluno']['matricula'])){
                $aluno = $this->Aluno->find('first',
                    array(
                        'conditions'=>array(
                            'Aluno.cpf LIKE'=> '%'.$this->request->data['Aluno']['cpf'].'%',
                            'Aluno.matricula LIKE '=> '%'.$this->request->data['Aluno']['matricula'].'%'
                        )
                    )
                );
                if(empty($aluno)){
                    $val_return['msg'] = 'CPF ou Matrícula inválidos';
                }
                else{
                    $val_return['status'] = 'true';
                    $val_return['msg'] = 'success';
                    $val_return['aluno'] = $aluno;
                    $this->Session->write('aluno_mobile',$aluno);
                }
            }
            else{
                $val_return['msg'] = 'CPF ou Matrícula inválidos';
            }

        }
        $result = Util::my_json_encode($val_return);
        //pr($val_return);
        $this->set(compact('result'));
        $this->render('json');
    }

    /**
     * Executa o logout
     */
    public function logout(){
        $this->Session->delete('aluno_mobile');
    }

    /**
     * Carrega o perfil do aluno pelo cpf
     * @param int $cpf
     */
    public function profile($cpf = 0){
        $this->layout = 'ajax';
        $val_return = array('status'=>'false','msg'=>'Você precisa estar logado');
        $aluno = $this->Aluno->findByCpf($cpf);
        $val_return['status'] = 'true';
        $val_return['msg'] = 'success';
        $val_return['aluno'] = $aluno;
        //pr($val_return);
        $result = Util::my_json_encode($val_return);
        $this->set(compact('result'));
        $this->render('json');
    }

} 