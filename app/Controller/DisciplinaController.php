<?php

App::uses('Util','Lib');
App::uses('URLify','Lib');

class DisciplinaController extends AppController{

    var $uses = array('Disciplina','Aluno');

    /**
     * Filtra as disciplinas do aluno por semestre
     * @param int $aluno
     * @param int $semestre
     */
    public function view($aluno = 0, $semestre = 0){
        $this->layout = 'ajax';
        $val_return = array('status'=>'false','msg'=>'Você precisa estar logado');

        if($aluno!=0 || $semestre!=0){
            $disciplinas = $this->Aluno->find(
                'all',
                array(
                    'conditions'=> array(
                        'Aluno.id'=>$aluno
                    ),
                    'contain'=> array(
                        'Disciplina' => array(
                            'conditions' => array('Disciplina.semestre '=> $semestre)
                        )
                    )
                )
            );

            if(!empty($disciplinas)){
                $val_return['status'] = 'true';
                $val_return['msg'] = 'success';
                $val_return['disciplinas'] = $disciplinas;
            }


            $result = Util::my_json_encode($val_return);
            //pr($val_return);
            $this->set(compact('result'));
            $this->render('json');
        }
    }
} 